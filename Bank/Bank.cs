﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;
using System.Xml;
using System.Text.RegularExpressions;
using System.Xml.XPath;

namespace TrigubDiblom
{
	public partial class Bank : Form
	{
		/// <summary>
		/// Файл XML-БД, з яким працює додаток
		/// </summary>
		XmlDocument dataBase = new XmlDocument();

		/// <summary>
		/// Конструктор за замовчуванням. Ініціалізує елементи форми.
		/// </summary>
		public Bank() => InitializeComponent();

		/// <summary>
		/// Перевіряє, чи є файл БД в папці. Якщо немає - то файл створюється.
		/// </summary>
		/// <returns>Ім'я файлу БД</returns>
		public static string GetDataBaseName()
		{
			// спроба завантажити файл БД
			if (!File.Exists("dataBase.xml"))
			{
				var xmlTW = new XmlTextWriter("dataBase.xml", Encoding.Unicode)
				{
					Formatting = Formatting.Indented,
					Indentation = 1,
					IndentChar = '\t'
				};

				xmlTW.WriteStartDocument();
				xmlTW.WriteStartElement("Bank");
				xmlTW.WriteEndElement();
				xmlTW.Close();
			}

			return "dataBase.xml";
		}

		/// <summary>
		/// Обробник події натискання на кнопку "Реєстрація"
		/// </summary>
		/// <param name="sender">Обов'язковий параметр обробника події</param>
		/// <param name="e">Обов'язковий параметр обробника події</param>
		private void bRegistration_Click(object sender, EventArgs e)
		{
			// регулярний вираз, за яким перевіряється допустимість введення імені та прізвища (тільки символи алфавіту)
			var regex = new Regex(@"[^A-Za-zА-Яа-я]");

			// якщо в імені або прізвищі є хоч один неалфавітний символ - помилка
			if (regex.IsMatch(this.tbName.Text) || regex.IsMatch(this.tbSurname.Text))
			{
				MessageBox.Show("Ім'я або прізвище містять недопустимі символи!", "Помилка вводу", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			// видаляє з номера телефону всі символи, крім '+' і цифр
			string phone = this.mtbPhone.Text.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "");

			// якщо такий номер телефону вже використовується - помилка
			if (!this.IsUnicValue("Phone", phone))
			{
				MessageBox.Show("Уведений номер телефону вже використовується!", "Помилка вводу", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			// інформація, що підлягає підпису - конкатенація номера телефону, імені та прізвища
			string userInfo = $"{phone}{tbName.Text}{tbSurname.Text}";

			/* номер карти визначається на основі даних про користувача. 
			 * Явне приведення до uint необхідно, щоб уникнути негативних значень, що призводять до неправильної роботи програми. 
			 * BitConverter.ToUInt32 (BitConverter.GetBytes (userInfo.GetHashCode ()), 0). Цей рядок необхідний виходячи з того, що одне і те ж
			 * двійкове число може бути інтерпритоване як позитивне, так і як негативне.
			 * Наприклад: 1010 1010 = -86 (для знакового типу) = 170 (для беззнакового). Нам необхідно, щоб число було беззнаковим, а метод GetHashCode () повертає знакове число, тому виконуємо перетворення.
			 * Оскільки в результаті ми можемо отримати число довжиною до 12 цифр, виконуємо його обрізання до перших 8, решта 8 сгенерируем випадково, щоб номери карт з більшою ймовірністю були унікальними
			 */
			string cardNum = BitConverter.ToUInt32(BitConverter.GetBytes(userInfo.GetHashCode()), 0).ToString().Substring(0, 8);

			// номер карти генерується, доки не стане унікальним
			do
			{
				this.GenerateCardNum(ref cardNum);
			} while (!this.IsUnicValue("CardNumber", cardNum));

			// створення запису про користувача в БД банку
			try
			{
				this.CreateUser(phone, cardNum, userInfo);
				MessageBox.Show("Клієнт успішно зареєстрований", "Успіх", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception)
			{
				MessageBox.Show($"Неможливо зареєствувати клієнта!{Environment.NewLine}Спробуйте ввести інші дані", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
			}
		}

		/// <summary>
		/// Додає дочірній елемент <c> nodeName </c> з вмістом <c> nodeValue </c> до елементу <c> parent </c>
		/// </summary>
		/// <param name="parent">Батьківський елемент, до якого виконується приєднання</param>
		/// <param name="nodeName">Ім'я дочірнього елемента</param>
		/// <param name="nodeValue">Вміст дочірнього елемента</param>
		private void AppendElement(XmlElement parent, string nodeName, string nodeValue)
		{
			XmlElement newNode = this.dataBase.CreateElement(nodeName);
			newNode.InnerText = nodeValue;
			parent.AppendChild(newNode);
		}

		/// <summary>
		/// Створює нового клієнта і додає його дані в БД
		/// </summary>
		/// <param name="phone">Номер телефону клиента</param>
		/// <param name="cardNum">Номер картки клиента</param>
		/// <param name="userInfo">Інформація про клієнта, що підлягає підпису</param>
		private void CreateUser(string phone, string cardNum, string userInfo)
		{
			// створюється екземпляр кріптосервісу RSA з довжиною ключа 1024 біт
			var rsa = new RSACryptoServiceProvider(1024);

			// параметри ключа RSA експортуються в окрему змінну
			RSAParameters rsaParams = rsa.ExportParameters(true);

			// створення нового клиенту в БД
			XmlElement newClient = this.dataBase.CreateElement("Client");
			this.dataBase.DocumentElement.AppendChild(newClient);

			// додавання інформації про користувача в БД
			AppendElement(newClient, "Phone", phone);
			AppendElement(newClient, "Name", this.tbName.Text);
			AppendElement(newClient, "Surname", this.tbSurname.Text);
			AppendElement(newClient, "CardNumber", cardNum);
			AppendElement(newClient, "EDS", Convert.ToBase64String(rsa.SignData(Encoding.Unicode.GetBytes(userInfo), new SHA1CryptoServiceProvider())));

			XmlElement newClientRsaKeyData = this.dataBase.CreateElement("RsaKeyData");
			AppendElement(newClientRsaKeyData, "Modulus", Convert.ToBase64String(rsaParams.Modulus));
			AppendElement(newClientRsaKeyData, "Exponent", Convert.ToBase64String(rsaParams.Exponent));
			AppendElement(newClientRsaKeyData, "D", Convert.ToBase64String(rsaParams.D));
			AppendElement(newClientRsaKeyData, "InverseQ", Convert.ToBase64String(rsaParams.InverseQ));
			AppendElement(newClientRsaKeyData, "DP", Convert.ToBase64String(rsaParams.DP));
			AppendElement(newClientRsaKeyData, "DQ", Convert.ToBase64String(rsaParams.DQ));
			AppendElement(newClientRsaKeyData, "P", Convert.ToBase64String(rsaParams.P));
			AppendElement(newClientRsaKeyData, "Q", Convert.ToBase64String(rsaParams.Q));

			newClient.AppendChild(newClientRsaKeyData);

			this.dataBase.Save("dataBase.xml");
		}

		/// <summary>
		/// Перевіряє, чи унікальне значення <c>value2check</c> в елементі <c>nodeName</c>
		/// </summary>
		/// <param name="nodeName">Ім'я вузла, вміст якого перевіряється</param>
		/// <param name="value2check">Значення, що перевіряється на унікальність</param>
		/// <returns>true - якщо значення унікально. Інакше - false</returns>
		private bool IsUnicValue(string nodeName, string value2check)
		{
			// вибираються nodeName всіх записів в БД
			XPathNodeIterator xpathIterator = new XPathDocument("dataBase.xml").CreateNavigator().Select($"Bank/Client/{nodeName}");

			foreach (object current in xpathIterator)
			{
				if (current.ToString() == value2check)
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Генерує номер карти, використовуючи генератор випадкових чисел
		/// </summary>
		/// <param name="cardNum"></param>
		private void GenerateCardNum(ref string cardNum)
		{
			var rand = new Random();
			var sb = new StringBuilder(cardNum);
			while (sb.Length < 16)
			{
				sb.Append(rand.Next(10)); // 0-9
			}
			cardNum = sb.ToString();
		}

		/// <summary>
		/// Обробник події завантаження форми
		/// </summary>
		/// <param name="sender">Обов'язковий параметр обробника події</param>
		/// <param name="e">Обов'язковий параметр обробника події</param>
		private void Bank_Load(object sender, EventArgs e)
			=> this.dataBase.Load(GetDataBaseName());


	}
}
