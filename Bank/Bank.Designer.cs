﻿namespace TrigubDiblom
{
	partial class Bank
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.bRegistration = new System.Windows.Forms.Button();
			this.mtbPhone = new System.Windows.Forms.MaskedTextBox();
			this.tbName = new System.Windows.Forms.TextBox();
			this.tbSurname = new System.Windows.Forms.TextBox();
			this.lTelephoneNumber = new System.Windows.Forms.Label();
			this.lName = new System.Windows.Forms.Label();
			this.lSurname = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// bRegistration
			// 
			this.bRegistration.Location = new System.Drawing.Point(124, 135);
			this.bRegistration.Name = "bRegistration";
			this.bRegistration.Size = new System.Drawing.Size(100, 23);
			this.bRegistration.TabIndex = 0;
			this.bRegistration.Text = "Зареєструвати";
			this.bRegistration.UseVisualStyleBackColor = true;
			this.bRegistration.Click += new System.EventHandler(this.bRegistration_Click);
			// 
			// mtbPhone
			// 
			this.mtbPhone.Location = new System.Drawing.Point(124, 33);
			this.mtbPhone.Mask = "+38(000) 000-00-00";
			this.mtbPhone.Name = "mtbPhone";
			this.mtbPhone.Size = new System.Drawing.Size(163, 20);
			this.mtbPhone.TabIndex = 1;
			// 
			// tbName
			// 
			this.tbName.Location = new System.Drawing.Point(124, 63);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(163, 20);
			this.tbName.TabIndex = 2;
			// 
			// tbSurname
			// 
			this.tbSurname.Location = new System.Drawing.Point(124, 93);
			this.tbSurname.Name = "tbSurname";
			this.tbSurname.Size = new System.Drawing.Size(163, 20);
			this.tbSurname.TabIndex = 3;
			// 
			// lTelephoneNumber
			// 
			this.lTelephoneNumber.Location = new System.Drawing.Point(19, 34);
			this.lTelephoneNumber.Name = "lTelephoneNumber";
			this.lTelephoneNumber.Size = new System.Drawing.Size(100, 18);
			this.lTelephoneNumber.TabIndex = 5;
			this.lTelephoneNumber.Text = "Номер телефону";
			// 
			// lName
			// 
			this.lName.Location = new System.Drawing.Point(84, 67);
			this.lName.Name = "lName";
			this.lName.Size = new System.Drawing.Size(35, 13);
			this.lName.TabIndex = 6;
			this.lName.Text = "Ім\'я";
			// 
			// lSurname
			// 
			this.lSurname.Location = new System.Drawing.Point(63, 96);
			this.lSurname.Name = "lSurname";
			this.lSurname.Size = new System.Drawing.Size(56, 14);
			this.lSurname.TabIndex = 7;
			this.lSurname.Text = "Прізвище";
			// 
			// Bank
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(352, 188);
			this.Controls.Add(this.lSurname);
			this.Controls.Add(this.lName);
			this.Controls.Add(this.lTelephoneNumber);
			this.Controls.Add(this.tbSurname);
			this.Controls.Add(this.tbName);
			this.Controls.Add(this.mtbPhone);
			this.Controls.Add(this.bRegistration);
			this.Name = "Bank";
			this.Text = "Банк";
			this.Load += new System.EventHandler(this.Bank_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button bRegistration;
		private System.Windows.Forms.MaskedTextBox mtbPhone;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.TextBox tbSurname;
		private System.Windows.Forms.Label lTelephoneNumber;
		private System.Windows.Forms.Label lName;
		private System.Windows.Forms.Label lSurname;
	}
}

