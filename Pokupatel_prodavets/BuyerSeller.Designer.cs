﻿namespace TrigubDiblom
{
	partial class BuyerSeller
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}
		#region Код, автоматически созданный конструктором форм Windows
		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tpBuyer = new System.Windows.Forms.TabPage();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.bSend2Seller = new System.Windows.Forms.Button();
			this.bEncryptDesKey = new System.Windows.Forms.Button();
			this.tbEncryptedDesKey = new System.Windows.Forms.TextBox();
			this.tbDesKey = new System.Windows.Forms.TextBox();
			this.tbBuyerCardNumber = new System.Windows.Forms.TextBox();
			this.tbBuyerNameSurname = new System.Windows.Forms.TextBox();
			this.bIdentificate = new System.Windows.Forms.Button();
			this.mtbPhone = new System.Windows.Forms.MaskedTextBox();
			this.tpSeller = new System.Windows.Forms.TabPage();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.lDesKey = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.bVerifyAndSell = new System.Windows.Forms.Button();
			this.bGetBankEds = new System.Windows.Forms.Button();
			this.tbBankEds = new System.Windows.Forms.TextBox();
			this.tbClientEds = new System.Windows.Forms.TextBox();
			this.tbSellerCardNumber = new System.Windows.Forms.TextBox();
			this.tbDecryptedDesKey = new System.Windows.Forms.TextBox();
			this.tbSellerNameSurname = new System.Windows.Forms.TextBox();
			this.bDecryptKey = new System.Windows.Forms.Button();
			this.tabControl1.SuspendLayout();
			this.tpBuyer.SuspendLayout();
			this.tpSeller.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tpBuyer);
			this.tabControl1.Controls.Add(this.tpSeller);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(522, 352);
			this.tabControl1.TabIndex = 0;
			// 
			// tpBuyer
			// 
			this.tpBuyer.Controls.Add(this.label5);
			this.tpBuyer.Controls.Add(this.label4);
			this.tpBuyer.Controls.Add(this.label3);
			this.tpBuyer.Controls.Add(this.label2);
			this.tpBuyer.Controls.Add(this.label1);
			this.tpBuyer.Controls.Add(this.bSend2Seller);
			this.tpBuyer.Controls.Add(this.bEncryptDesKey);
			this.tpBuyer.Controls.Add(this.tbEncryptedDesKey);
			this.tpBuyer.Controls.Add(this.tbDesKey);
			this.tpBuyer.Controls.Add(this.tbBuyerCardNumber);
			this.tpBuyer.Controls.Add(this.tbBuyerNameSurname);
			this.tpBuyer.Controls.Add(this.bIdentificate);
			this.tpBuyer.Controls.Add(this.mtbPhone);
			this.tpBuyer.Location = new System.Drawing.Point(4, 22);
			this.tpBuyer.Name = "tpBuyer";
			this.tpBuyer.Padding = new System.Windows.Forms.Padding(3);
			this.tpBuyer.Size = new System.Drawing.Size(514, 326);
			this.tpBuyer.TabIndex = 0;
			this.tpBuyer.Text = "Покупець";
			this.tpBuyer.UseVisualStyleBackColor = true;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(15, 206);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(137, 13);
			this.label5.TabIndex = 12;
			this.label5.Text = "Зашифрований ключ DES";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(15, 159);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(58, 13);
			this.label4.TabIndex = 11;
			this.label4.Text = "Ключ DES";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(15, 112);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(73, 13);
			this.label3.TabIndex = 10;
			this.label3.Text = "Номер карти";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(15, 65);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(91, 13);
			this.label2.TabIndex = 9;
			this.label2.Text = "Прізвище та ім\'я";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(15, 18);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(92, 13);
			this.label1.TabIndex = 8;
			this.label1.Text = "Номер телефону";
			// 
			// bSend2Seller
			// 
			this.bSend2Seller.Enabled = false;
			this.bSend2Seller.Location = new System.Drawing.Point(129, 257);
			this.bSend2Seller.Name = "bSend2Seller";
			this.bSend2Seller.Size = new System.Drawing.Size(104, 41);
			this.bSend2Seller.TabIndex = 7;
			this.bSend2Seller.Text = "Відправити дані продавцю";
			this.bSend2Seller.UseVisualStyleBackColor = true;
			this.bSend2Seller.Click += new System.EventHandler(this.bSend2Seller_Click);
			// 
			// bEncryptDesKey
			// 
			this.bEncryptDesKey.Enabled = false;
			this.bEncryptDesKey.Location = new System.Drawing.Point(184, 175);
			this.bEncryptDesKey.Name = "bEncryptDesKey";
			this.bEncryptDesKey.Size = new System.Drawing.Size(152, 26);
			this.bEncryptDesKey.TabIndex = 6;
			this.bEncryptDesKey.Text = "Зашифрувати ключ DES";
			this.bEncryptDesKey.UseVisualStyleBackColor = true;
			this.bEncryptDesKey.Click += new System.EventHandler(this.bEncryptDesKey_Click);
			// 
			// tbEncryptedDesKey
			// 
			this.tbEncryptedDesKey.Location = new System.Drawing.Point(15, 226);
			this.tbEncryptedDesKey.Name = "tbEncryptedDesKey";
			this.tbEncryptedDesKey.ReadOnly = true;
			this.tbEncryptedDesKey.Size = new System.Drawing.Size(338, 20);
			this.tbEncryptedDesKey.TabIndex = 5;
			// 
			// tbDesKey
			// 
			this.tbDesKey.Location = new System.Drawing.Point(15, 179);
			this.tbDesKey.Name = "tbDesKey";
			this.tbDesKey.Size = new System.Drawing.Size(151, 20);
			this.tbDesKey.TabIndex = 4;
			this.tbDesKey.TextChanged += new System.EventHandler(this.tbDesKey_TextChanged);
			// 
			// tbBuyerCardNumber
			// 
			this.tbBuyerCardNumber.Location = new System.Drawing.Point(15, 132);
			this.tbBuyerCardNumber.Name = "tbBuyerCardNumber";
			this.tbBuyerCardNumber.ReadOnly = true;
			this.tbBuyerCardNumber.Size = new System.Drawing.Size(151, 20);
			this.tbBuyerCardNumber.TabIndex = 3;
			// 
			// tbBuyerNameSurname
			// 
			this.tbBuyerNameSurname.Location = new System.Drawing.Point(15, 85);
			this.tbBuyerNameSurname.Name = "tbBuyerNameSurname";
			this.tbBuyerNameSurname.ReadOnly = true;
			this.tbBuyerNameSurname.Size = new System.Drawing.Size(151, 20);
			this.tbBuyerNameSurname.TabIndex = 2;
			// 
			// bIdentificate
			// 
			this.bIdentificate.Location = new System.Drawing.Point(184, 37);
			this.bIdentificate.Name = "bIdentificate";
			this.bIdentificate.Size = new System.Drawing.Size(99, 23);
			this.bIdentificate.TabIndex = 1;
			this.bIdentificate.Text = "Ідентифікація";
			this.bIdentificate.UseVisualStyleBackColor = true;
			this.bIdentificate.Click += new System.EventHandler(this.bIdentificate_Click);
			// 
			// mtbPhone
			// 
			this.mtbPhone.Location = new System.Drawing.Point(15, 38);
			this.mtbPhone.Mask = "+38(000) 000-00-00";
			this.mtbPhone.Name = "mtbPhone";
			this.mtbPhone.Size = new System.Drawing.Size(116, 20);
			this.mtbPhone.TabIndex = 0;
			// 
			// tpSeller
			// 
			this.tpSeller.Controls.Add(this.bDecryptKey);
			this.tpSeller.Controls.Add(this.label10);
			this.tpSeller.Controls.Add(this.label9);
			this.tpSeller.Controls.Add(this.label8);
			this.tpSeller.Controls.Add(this.lDesKey);
			this.tpSeller.Controls.Add(this.label6);
			this.tpSeller.Controls.Add(this.bVerifyAndSell);
			this.tpSeller.Controls.Add(this.bGetBankEds);
			this.tpSeller.Controls.Add(this.tbBankEds);
			this.tpSeller.Controls.Add(this.tbClientEds);
			this.tpSeller.Controls.Add(this.tbSellerCardNumber);
			this.tpSeller.Controls.Add(this.tbDecryptedDesKey);
			this.tpSeller.Controls.Add(this.tbSellerNameSurname);
			this.tpSeller.Location = new System.Drawing.Point(4, 22);
			this.tpSeller.Name = "tpSeller";
			this.tpSeller.Padding = new System.Windows.Forms.Padding(3);
			this.tpSeller.Size = new System.Drawing.Size(514, 326);
			this.tpSeller.TabIndex = 1;
			this.tpSeller.Text = "Продавець";
			this.tpSeller.UseVisualStyleBackColor = true;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(8, 233);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(140, 13);
			this.label10.TabIndex = 11;
			this.label10.Text = "ЕЦП, отриманий від банку";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(8, 176);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(152, 13);
			this.label9.TabIndex = 10;
			this.label9.Text = "ЕЦП, отриманий від покупця";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(8, 119);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(117, 13);
			this.label8.TabIndex = 9;
			this.label8.Text = "Номер карти покупця";
			// 
			// lDesKey
			// 
			this.lDesKey.AutoSize = true;
			this.lDesKey.Location = new System.Drawing.Point(8, 62);
			this.lDesKey.Name = "lDesKey";
			this.lDesKey.Size = new System.Drawing.Size(143, 13);
			this.lDesKey.TabIndex = 8;
			this.lDesKey.Text = "Розшифрований ключ DES";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(8, 5);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(135, 13);
			this.label6.TabIndex = 7;
			this.label6.Text = "Прізвище та ім\'я покупця";
			// 
			// bVerifyAndSell
			// 
			this.bVerifyAndSell.Location = new System.Drawing.Point(215, 290);
			this.bVerifyAndSell.Name = "bVerifyAndSell";
			this.bVerifyAndSell.Size = new System.Drawing.Size(200, 23);
			this.bVerifyAndSell.TabIndex = 6;
			this.bVerifyAndSell.Text = "Перевірити ЕЦП і відпустити товар";
			this.bVerifyAndSell.UseVisualStyleBackColor = true;
			this.bVerifyAndSell.Click += new System.EventHandler(this.bVerifyAndSell_Click);
			// 
			// bGetBankEds
			// 
			this.bGetBankEds.Location = new System.Drawing.Point(8, 290);
			this.bGetBankEds.Name = "bGetBankEds";
			this.bGetBankEds.Size = new System.Drawing.Size(96, 23);
			this.bGetBankEds.TabIndex = 5;
			this.bGetBankEds.Text = "Отримати ЕЦП клієнта від банку";
			this.bGetBankEds.UseVisualStyleBackColor = true;
			this.bGetBankEds.Click += new System.EventHandler(this.bGetBankEds_Click);
			// 
			// tbBankEds
			// 
			this.tbBankEds.Location = new System.Drawing.Point(8, 258);
			this.tbBankEds.Name = "tbBankEds";
			this.tbBankEds.ReadOnly = true;
			this.tbBankEds.Size = new System.Drawing.Size(498, 20);
			this.tbBankEds.TabIndex = 4;
			// 
			// tbClientEds
			// 
			this.tbClientEds.Location = new System.Drawing.Point(8, 201);
			this.tbClientEds.Name = "tbClientEds";
			this.tbClientEds.ReadOnly = true;
			this.tbClientEds.Size = new System.Drawing.Size(498, 20);
			this.tbClientEds.TabIndex = 3;
			// 
			// tbSellerCardNumber
			// 
			this.tbSellerCardNumber.Location = new System.Drawing.Point(8, 144);
			this.tbSellerCardNumber.Name = "tbSellerCardNumber";
			this.tbSellerCardNumber.ReadOnly = true;
			this.tbSellerCardNumber.Size = new System.Drawing.Size(175, 20);
			this.tbSellerCardNumber.TabIndex = 2;
			// 
			// tbDecryptedDesKey
			// 
			this.tbDecryptedDesKey.Location = new System.Drawing.Point(8, 87);
			this.tbDecryptedDesKey.Name = "tbDecryptedDesKey";
			this.tbDecryptedDesKey.ReadOnly = true;
			this.tbDecryptedDesKey.Size = new System.Drawing.Size(175, 20);
			this.tbDecryptedDesKey.TabIndex = 1;
			// 
			// tbSellerNameSurname
			// 
			this.tbSellerNameSurname.Location = new System.Drawing.Point(8, 30);
			this.tbSellerNameSurname.Name = "tbSellerNameSurname";
			this.tbSellerNameSurname.ReadOnly = true;
			this.tbSellerNameSurname.Size = new System.Drawing.Size(175, 20);
			this.tbSellerNameSurname.TabIndex = 0;
			// 
			// bDecryptKey
			// 
			this.bDecryptKey.Location = new System.Drawing.Point(268, 80);
			this.bDecryptKey.Name = "bDecryptKey";
			this.bDecryptKey.Size = new System.Drawing.Size(147, 35);
			this.bDecryptKey.TabIndex = 12;
			this.bDecryptKey.Text = "Розшифрувати ключ і номер карти";
			this.bDecryptKey.UseVisualStyleBackColor = true;
			this.bDecryptKey.Click += new System.EventHandler(this.bDecryptKey_Click);
			// 
			// BuyerSeller
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(522, 352);
			this.Controls.Add(this.tabControl1);
			this.Name = "BuyerSeller";
			this.Text = "Обмін інформацією між продавцем і покупцем";
			this.tabControl1.ResumeLayout(false);
			this.tpBuyer.ResumeLayout(false);
			this.tpBuyer.PerformLayout();
			this.tpSeller.ResumeLayout(false);
			this.tpSeller.PerformLayout();
			this.ResumeLayout(false);
		}
		#endregion
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tpBuyer;
		private System.Windows.Forms.MaskedTextBox mtbPhone;
		private System.Windows.Forms.TabPage tpSeller;
		private System.Windows.Forms.Button bSend2Seller;
		private System.Windows.Forms.Button bEncryptDesKey;
		private System.Windows.Forms.TextBox tbEncryptedDesKey;
		private System.Windows.Forms.TextBox tbDesKey;
		private System.Windows.Forms.TextBox tbBuyerCardNumber;
		private System.Windows.Forms.TextBox tbBuyerNameSurname;
		private System.Windows.Forms.Button bIdentificate;
		private System.Windows.Forms.Button bGetBankEds;
		private System.Windows.Forms.TextBox tbBankEds;
		private System.Windows.Forms.TextBox tbClientEds;
		private System.Windows.Forms.TextBox tbSellerCardNumber;
		private System.Windows.Forms.TextBox tbDecryptedDesKey;
		private System.Windows.Forms.TextBox tbSellerNameSurname;
		private System.Windows.Forms.Button bVerifyAndSell;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label lDesKey;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button bDecryptKey;
	}
}