﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.IO;

namespace TrigubDiblom
{
	public partial class BuyerSeller : Form
	{
		Client client; // поле, в якому зберігається інформація про поточного клієнта

		public BuyerSeller()
			=> InitializeComponent();

		#region Buyer

		/// <summary>
		/// Виконує ідентифікацію клієнта за номером телефону
		/// </summary>
		/// <param name="sender">Обов'язковий параметр обробника події</param>
		/// <param name="e">Обов'язковий параметр обробника події</param>
		private void bIdentificate_Click(object sender, EventArgs e)
		{
			var doc = XDocument.Load(Bank.GetDataBaseName());

			string phone = this.mtbPhone.Text // видаляє з номеру телефону символи, додані маскою ( символи дужок і тире )
				.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "");

			IEnumerable<XElement> client = doc.Descendants("Client")
				.Where(cl => cl.Element("Phone").Value.ToString() == phone);

			if (client.Count() == 0)
			{
				MessageBox.Show(this, "Клієнта з таким номером не існує в базі!",
					"Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			this.client = ReadClientData(client.First());
			this.tbBuyerNameSurname.Text = $"{this.client.Name} {this.client.Surname}";
			this.tbBuyerCardNumber.Text = this.client.CardNumber;

			this.mtbPhone.TextChanged += this.mtbPhone_TextChanged;
			(sender as Button).Enabled = false;
		}

		/// <summary>
		/// Зчитує дані клієнта банку, переданого у параметрі client
		/// </summary>
		/// <param name="client">Клієнт, дані якого зчитуються з БД</param>
		/// <returns>Екземпляр класу Client, який інкапсулює дані про поточного клієнта</returns>
		private Client ReadClientData(XElement client)
		{
			var res = new Client();
			object rsaParams = new RSAParameters();
			bool rsaBegins = false;

			foreach (XElement item in client.Descendants())
			{
				if (item.Name.ToString() == "RsaKeyData")
				{ // спочатку дані записуються безпосередньо у властивості екзмепляру класу Client, потім - у окрему зміну, яка інкапсулює параметри ключу RSA
					rsaBegins = true;
					continue;
				}
				try
				{
					if (!rsaBegins)
					{
						res.GetType().GetProperty(item.Name.ToString()).SetValue(res, item.Value.ToString(), null);
					}
					else
					{
						rsaParams.GetType().GetField(item.Name.ToString()).SetValue(rsaParams, Convert.FromBase64String(item.Value.ToString()));
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show(this, ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}

			res.RsaParams = (RSAParameters)rsaParams;

			return res;
		}

		/// <summary>
		/// Шифрує ключ DES
		/// </summary>
		/// <param name="sender">Обов'язковий параметр обробника події</param>
		/// <param name="e">Обов'язковий параметр обробника події</param>
		private void bEncryptDesKey_Click(object sender, EventArgs e)
		{
			var rsa = new RSACryptoServiceProvider(1024);
			rsa.ImportParameters(this.client.RsaParams);
			this.tbEncryptedDesKey.Text =
				Convert.ToBase64String(rsa.Encrypt(Encoding.Unicode.GetBytes(this.tbDesKey.Text), true));
			this.bSend2Seller.Enabled = true;
		}

		/// <summary>
		/// Шифрує номер карти клієнта.
		/// </summary>
		/// <param name="des">Екземпляр класу <code>DESCryptoServiceProvider</code>, з допомогою якого виконується шифрування номеру карти</param>
		/// <returns>Масив байтів зашифрованого номеру картки</returns>
		private byte[] EncryptCardNumber(DESCryptoServiceProvider des)
		{
			using (var ms = new MemoryStream())
			{
				using (var cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write))
				{
					using (var sw = new StreamWriter(cs))
					{
						sw.Write(this.client.CardNumber);
					}
					return ms.ToArray();
				}
			}
		}

		/// <summary>
		/// Відправляє зашифровані дані і ЕЦП продавцю
		/// </summary>
		/// <param name="sender">Обов'язковий параметр обробника події</param>
		/// <param name="e">Обов'язковий параметр обробника події</param>
		private void bSend2Seller_Click(object sender, EventArgs e)
		{
			(sender as Button).Enabled = false;

			byte[] passwordHash = MD5.Create()
				.ComputeHash(Encoding.Unicode.GetBytes(this.tbDesKey.Text));

			var des = new DESCryptoServiceProvider
			{
				Key = passwordHash.Take(8).ToArray(),
				IV = passwordHash.Reverse().Take(8).ToArray()
			};
			byte[] encryptedData = this.EncryptCardNumber(des);

			this.tbSellerCardNumber.Text = Convert.ToBase64String(encryptedData);
			this.lDesKey.Text = "Зашифрований ключ DES";
			this.tbDecryptedDesKey.Text = this.tbEncryptedDesKey.Text;
			this.tbSellerNameSurname.Text = $"{this.client.Name} {this.client.Surname}";

			var rsa = new RSACryptoServiceProvider(1024);
			rsa.ImportParameters(this.client.RsaParams);
			this.tbClientEds.Text =
				Convert.ToBase64String(rsa.SignData(
					Encoding.Unicode.GetBytes($"{this.client.Phone}{tbBuyerNameSurname.Text.Replace(" ", "")}"),
					new SHA1CryptoServiceProvider()));
		}

		/// <summary>
		/// Кнопка "Зашифрувати ключ DES" активна тільки коли цей ключ не є пустим рядком.
		/// </summary>
		/// <param name="sender">Обов'язковий параметр обробника події</param>
		/// <param name="e">Обов'язковий параметр обробника події</param>
		private void tbDesKey_TextChanged(object sender, EventArgs e)
			=> this.bEncryptDesKey.Enabled = (sender as TextBox).Text != String.Empty;

		/// <summary>
		/// При зміні поточного номеру телефону дані всіх полів очищуються
		/// </summary>
		/// <param name="sender">Обов'язковий параметр обробника події</param>
		/// <param name="e">Обов'язковий параметр обробника події</param>
		private void mtbPhone_TextChanged(object sender, EventArgs e)
		{
			this.tbBuyerNameSurname.Text =
				this.tbBuyerCardNumber.Text =
				this.tbDesKey.Text =
				this.tbEncryptedDesKey.Text =
				this.tbSellerNameSurname.Text =
				this.tbDecryptedDesKey.Text =
				this.tbSellerCardNumber.Text =
				this.tbClientEds.Text =
				this.tbBankEds.Text = String.Empty;
			this.bIdentificate.Enabled = true;
			(sender as MaskedTextBox).TextChanged -= this.mtbPhone_TextChanged;
		}

		#endregion

		#region Seller

		/// <summary>
		/// Розшифровує ключ DES і номер картки клієнта
		/// </summary>
		/// <param name="sender">Обов'язковий параметр обробника події</param>
		/// <param name="e">Обов'язковий параметр обробника події</param>
		private void bDecryptKey_Click(object sender, EventArgs e)
		{
			var rsa = new RSACryptoServiceProvider(1024);
			rsa.ImportParameters(this.client.RsaParams);
			this.lDesKey.Text = "Розшифрований ключ DES";
			this.tbDecryptedDesKey.Text =
				Encoding.Unicode.GetString(rsa.Decrypt(Convert.FromBase64String(this.tbDecryptedDesKey.Text), true));

			byte[] passwordHash = MD5.Create()
				.ComputeHash(Encoding.Unicode.GetBytes(this.tbDecryptedDesKey.Text));
			var des = new DESCryptoServiceProvider
			{
				Key = passwordHash.Take(8).ToArray(),
				IV = passwordHash.Reverse().Take(8).ToArray()
			};

			this.tbSellerCardNumber.Text = this.DecryptCartNumber(des);
		}

		/// <summary>
		/// Розшифровує номер картки клієнта
		/// </summary>
		/// <param name="des">Екземпляр класу <code>DESCryptoServiceProvider</code>, з допомогою якого виконується розшифрування номеру карти</param>
		/// <returns>Розшифрований номер карти поточного клієнта</returns>
		private string DecryptCartNumber(DESCryptoServiceProvider des)
		{
			using (var ms = new MemoryStream(Convert.FromBase64String(this.tbSellerCardNumber.Text)))
			{
				using (var cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Read))
				{
					using (var sr = new StreamReader(cs))
					{
						return sr.ReadToEnd();
					}
				}
			}
		}

		/// <summary>
		/// Отримує ЕЦП поточного користувача з БД банку
		/// </summary>
		/// <param name="sender">Обов'язковий параметр обробника події</param>
		/// <param name="e">Обов'язковий параметр обробника події</param>
		private void bGetBankEds_Click(object sender, EventArgs e)
		{
			var xdoc = XDocument.Load(Bank.GetDataBaseName());
			IEnumerable<XElement> client = xdoc.Descendants("Client") // знаходимо клієнта із зазначеним номером карти
				.Where(cl => cl.Element("CardNumber").Value.ToString() == this.tbSellerCardNumber.Text);

			if (client.Count() == 0)
			{
				MessageBox.Show(this, "Клієнта з таким номером карти не існує!",
					"Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			this.tbBankEds.Text = client.First()
				.Descendants("EDS").First()
				.Value.ToString();
		}

		/// <summary>
		/// Верифікація ЕЦП
		/// </summary>
		/// <param name="sender">Обов'язковий параметр обробника події</param>
		/// <param name="e">Обов'язковий параметр обробника події</param>
		private void bVerifyAndSell_Click(object sender, EventArgs e)
		{
			// якщо ЕЦП не збігаються - помилка
			if (this.tbBankEds.Text != this.tbClientEds.Text)
			{
				MessageBox.Show(this, "ЕЦП не співпадають! Дані пошкоджені!",
					"Дані пошкоджені!", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			MessageBox.Show(this, "Дані не були пошкоджені або модифіковані!",
				"Операція успішна!", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		#endregion
	}

	/// <summary>
	/// Клас, що інкапсулює дані щодо поточного клієнта.
	/// </summary>
	public class Client
	{
		public string Phone { get; set; }
		public string Name { get; set; }
		public string Surname { get; set; }
		public string CardNumber { get; set; }
		public string EDS { get; set; }
		public RSAParameters RsaParams { get; set; }
	}
}